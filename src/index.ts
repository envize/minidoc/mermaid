import { Flow as FlowBase, FlowType, StepType } from "@minidoc/core";

class Flow extends FlowBase {
    id: string = null;
}

export function plot(flows: FlowBase[]): string {
    return new Plotter().plot(flows);
}

class Plotter {
    private content: string;
    private happyFlow: Flow = null;
    private alternateFlows: Flow[] = [];

    public plot(input: FlowBase[]): string {
        const flows = this.sanitizeFlows(input);

        this.content = 'flowchart\n';

        for (const flow of flows) {
            this.registerFlowSteps(flow);
        }

        if (flows.length > 0) {
            this.happyFlow = flows.find(f => f.type === FlowType.HappyFlow);
            this.alternateFlows = flows.filter(f => f !== this.happyFlow);

            if (flows.length == 1) {
                this.linkFlowInternals(flows[0]);
            } else {
                for (const flow of flows) {
                    this.createSubgraph(flow);
                }
            }
        }

        return this.content;
    }

    private sanitizeFlows(input: FlowBase[]): Flow[] {
        const flows: Flow[] = [];

        for (let i = 0; i < input.length; i++) {
            const flow = <Flow> Object.create(input[i]);
            Object.assign(flow, input[i]);
            flow.id = `${flow.type}${(i + 1)}`;
            flows.push(flow);
        }

        return flows;
    }

    private getStep(type: StepType, description: string) {
        switch (type) {
        case StepType.Default:
            return `("${description}")`;
        case StepType.Action:
            return `[/"${description}"/]`;
        }
    }

    private registerFlowSteps(flow: Flow) {
        for (const step of flow.steps) {
            this.content += `${flow.id}.${step.id}${this.getStep(step.type, step.description)}\n`;
        }

        this.content += '\n';
    }

    private createSubgraph(flow: Flow) {
        this.content += `subgraph ${flow.id}["${flow.description ?? ' '}"]\n`;
        if (flow === this.happyFlow) {
            this.content += `style ${flow.id} fill:transparent,stroke-width:0\n`;
        }
        this.content += 'direction TB\n\n';

        this.linkFlowInternals(flow);

        if (flow === this.happyFlow) {
            for (const flow of this.alternateFlows) {
                this.linkFlow(flow);
            }
        }

        this.content += 'end\n\n';
    }

    private linkFlowInternals(flow: Flow) {
        this.content += `${flow.steps.map(s => `${flow.id}.${s.id}`).join(' --> ')}\n`;
    }

    private linkFlow(alternateFlow: Flow) {
        if (alternateFlow.start &&
            alternateFlow.end &&
            alternateFlow.start.target?.id === alternateFlow.end.target?.id) {

            this.content += `${this.happyFlow.id}.${alternateFlow.start.target.id} <-.-> ${alternateFlow.id}\n`;
            return;
        }

        if (alternateFlow.start) {
            this.content += `${this.happyFlow.id}.${alternateFlow.start.target.id} -.-> ${alternateFlow.id}\n`;
        }

        if (alternateFlow.end) {
            this.content += `${alternateFlow.id}-.-> ${this.happyFlow.id}.${alternateFlow.end.target.id}\n`;
        }
    }
}
